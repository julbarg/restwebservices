# RESTful Web Services

Social Media Application

Users -> Posts

- Retrieve all Users    - GET /users
- Create a User         - POST /users
- Retrieve one User     - GET /users/{id}
- Delete a User         - DELETE /users/{id}

- Retrieve all post for a User  - GET /users/{id}/posts
- Create a posts for a User     - POST /users/{id}/posts
- Retrieve details of a post    - GET /users/{id}/post/{post_id}

#### Internationalization

##### Configuration
- LocaleResolver
    - Default Locale - Locale.US
- ResourceBundleMessageSource

##### Usage
- Autowire MessageSource
- @RequestHeader(value = "Accept-Language", required = false) Locale locale
- messageSource.getMessage("helloWorld.message", null, locale)