package com.julian.rest.webservices.restfulwebservices.filtering;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@JsonFilter("SomeBeanFilter")
// @JsonIgnoreProperties(value = {"field1", "filed2"}) - Static filtering
public class SomeBean {

    private String field1;

    private String field2;

    //  @JsonIgnore
    private String field3;
}
